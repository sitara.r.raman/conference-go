import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    response = requests.get(url, header=header)
    return response.json()["photos"][0]["url"]


def get_weather_data(city, state):
    params = {"q": f"{city}, {state},US", "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()

    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()

    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]
        return {"temp": temp, "description": desc}
    except (KeyError, IndexError):
        return None
